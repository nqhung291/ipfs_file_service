const chokidar = require('chokidar');
const path = require('path');
const properties = require('../common/properties.js');
const userStore = require('../service/UserStore');
const filesData = require('../service/FileStore');

// This module is used to tracking changes in IPFS Folder
class Watcher {
  constructor(ipfs) {
    this.node = ipfs;
    this.watcher = null;
  }

  async watchFile() {
    try {
      console.log('User Store: ', userStore.getActiveUser());
      this.userFolder = await path.join(properties.defaultDir, userStore.getActiveUser());
    } catch (e) {
      console.log(e);
    }

    this.watcher = chokidar.watch(this.userFolder, {
      ignoreInitial: true,
      persistent: false,
      usePolling: true,
      awaitWriteFinish: true,
    });

    // when a file is added, push to ipfs network
    this.watcher.on('add', (filePath) => {
      const parentPath = path.dirname(filePath);
      if (parentPath === this.userFolder) {
        this.node.pushFileToIPFS(filePath);
      }
    });


    this.watcher.on('unlink', async (filePath) => {
      try {
        const nameFile = path.basename(filePath);
        console.log(nameFile);
        const file = filesData.deleteFile(nameFile, userStore.getActiveUser());
        filesData.deleteFileServer(nameFile, userStore.getActiveUser());
        console.log('Deleted:  ', nameFile);
      } catch (e) {
        console.log('Error Delete File');
        console.log(e);
      }
    });

    // on change
    this.watcher.on('change', async (filePath) => {
      console.log('Changing: ', filePath);
      try {
        this.node.pushFileToIPFS(filePath);
      } catch (e) {
        console.log(e);
      }
    });
  }

  stop() {
    if (this.watcher) {
      this.watcher.close();
      console.log('Stopped watcher');
    }
  }
}

module.exports = Watcher;
