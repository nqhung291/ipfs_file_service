const IPFS = require('ipfs');
const multiaddr = require('multiaddr');
const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const cryptoConst = require('../common/cryptoConst');
const properties = require('../common/properties.js');
const FileUtils = require('../common/FileUtils');
const userStore = require('../service/UserStore');
const filesData = require('../service/FileStore');

class Ipfs {
  constructor() {
    this.node = null;
  }

  async start() {
    try {
      this.userFolder = await path.join(properties.defaultDir, userStore.getActiveUser());
      console.log(this.userFolder);
      this.node = await IPFS.create();
      properties.ipfsPeers.map((address) => multiaddr(address)).forEach(async (multiAddr) => {
        try {
          await this.node.swarm.connect(multiAddr);
          await this.node.bootstrap.add(multiAddr);
          console.log('Successfully connected: ', multiAddr.toString());
        } catch (e) {
          console.log(`Cannot connect address: ${multiAddr.toString()}`);
        }
      });
    } catch (e) {
      console.log(e);
    }
  }

  pushFileToIPFS(filePath) {
    try {
      const parentPath = path.dirname(filePath);
      if (parentPath === this.userFolder) {
        const fileEncryptPath = path.format({
          dir: properties.saveZipFolder,
          name: path.basename(filePath),
          ext: '.enc',
        });
        FileUtils.encryptFile(filePath, userStore.getActiveUser(), fileEncryptPath)
          .then(async (key) => {
            try {
              const result = await this.node.addFromFs(fileEncryptPath); // push to ipfs
              // await this.node.pin.add(result[0].hash);
              // todo: save file to mongodb
              const mapFile = {
                cid: result[0].hash,
                name: path.basename(result[0].path, '.enc'),
                type: path.extname(path.basename(result[0].path, '.enc')),
                size: result[0].size,
                hash_key: key,
                owner: userStore.getActiveUser(),
                user: [],
                isDeleted: false,
              };
              FileUtils.saveToDB(mapFile); // save file to local db
              filesData.createFileServer(mapFile);
            } catch (e) {
              console.log(e);
            }
          });
      }
    } catch (e) {
      console.log(e);
    }
  }

  getFileFromIPFS(file) {
    try {
      const decryptKey = crypto.createHash('sha256').update(file.hash_key).digest();
      const ipfsStream = this.node.getReadableStream(file.cid);
      const output = fs.createWriteStream(path.format({
        dir: this.userFolder,
        name: file.cid,
        ext: path.extname(file.name),
      }));
      ipfsStream
        .pipe(crypto.createDecipheriv(cryptoConst.algorithm, decryptKey, cryptoConst.iv))
        .pipe(output);
    } catch (e) {
      console.log(e);
    }
  }

  stop() {
    if (this.node) {
      this.node.stop();
      console.log('Stopped IPFS node');
    }
  }
}
const ipfs = new Ipfs();
module.exports = ipfs;
