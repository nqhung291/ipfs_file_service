const { ipcRenderer } = require('electron');
let activeUser = require('../service/UserStore.js');
let {BrowserWindow} = require("electron").remote;
let path = require('path');
let app = require('electron').remote;

window.onload = () => {
  // get Name and ava
  const email = activeUser.get('activeEmail');
  const userInfo = activeUser.findUserByEmail(email)[0];
  // console.log(userInfo);

  let htmlUser = '<span class="avatar avatar-sm rounded-circle">';
  htmlUser += '<img alt="Image placeholder" src="';
  htmlUser += userInfo.picture;
  htmlUser += '"></span>';
  htmlUser += '<div class="media-body ml-2 d-none d-lg-block">';
  htmlUser += '<span class="mb-0 text-sm  font-weight-bold">';
  htmlUser += userInfo.name;
  htmlUser += '</span></div>';
  this.document.getElementById('name-and-avatar').innerHTML = htmlUser;
  console.log(htmlUser);
};
