const { ipcRenderer } = require('electron');

window.onload = () => {
  document.getElementById('logout').addEventListener('click', () => {
    // document.location.href = 'https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://www.youtbe.com';
    ipcRenderer.send('log-out');
  });
  ipcRenderer.on('logout', () => {
    window.location = 'https://mail.google.com/mail/u/0/?logout&hl=en';
  });
};
