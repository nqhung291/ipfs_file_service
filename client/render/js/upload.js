const {
  dialog
} = require('electron').remote;
const userStore = require('../service/UserStore.js');

// upload file
document.getElementById('upload-file').addEventListener('click', () => {
  dialog.showOpenDialog((fileNames) => {
    if (fileNames === undefined) {
      console.log('No file selected');
    } else {
      console.log(fileNames[0]); // chosen file path( file upload) = fileNames[0]
      const file_name = fileNames[0].substring(
        fileNames[0].lastIndexOf('/'),
        fileNames[0].length,
      );
      console.log(file_name);
      const filePath = `/home/${user}/ipfsbox/${userStore.getActiveUser()}/${file_name}`;
      // destination will be created or overwritten by default.
      fs.copyFile(fileNames[0], filePath, (err) => {
        if (err) throw err;
        Swal.fire({
          icon: 'success',
          title: 'Upload File Successful !',
        }).then((result) => {
          if (result.value) {
            location.reload();
          }
        });
      });
    }
  });
});