const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const crypto = require('crypto');
const archiver = require('archiver');
// const { sessionStorage } = require('electron-browser-storage');
const properties = require('./properties.js');
const cryptoConst = require('./cryptoConst');
const FileStore = require('../service/FileStore');
const userStore = require('../service/UserStore');

module.exports = {
  getPathToZip(currentPath) {
    const parentPath = path.dirname(currentPath);
    if (parentPath === properties.defaultDir) {
      return currentPath;
    }
    return this.getPathToZip(parentPath);
  },

  // async transformFolder(folderPath, zipFolderPath) {
  //   const folderName = path.basename(folderPath);
  //   const folderList = FileStore.findFileByName(path.format({
  //     name: folderName,
  //     ext: '.zip',
  //   }));
  //   let key;
  //   if (folderList != null && folderList.length > 0) {
  //     key = folderList[0].hash_key;
  //   } else {
  //     key = Math.random().toString(36).substring(2, 15);
  //   }

  //   const encryptKey = crypto.createHash('sha256').update(key).digest();
  //   try {
  //     const archive = archiver('zip', {
  //       zlib: { level: 9 },
  //     });
  //     const output = fs.createWriteStream(zipFolderPath);
  //     await archive
  //       .pipe(crypto.createCipheriv(cryptoConst.algorithm, encryptKey, cryptoConst.iv))
  //       .pipe(output);
  //     await archive.directory(folderPath, path.basename(folderPath, path.extname(folderPath)));
  //     await archive.finalize();
  //   } catch (e) {
  //     console.log(e);
  //   }
  //   return key;
  // },

  async encryptFile(filePath, email, fileEncryptPath) {
    const fileName = path.basename(filePath);
    const fileList = FileStore.findFileByNameAndEmail(fileName, email);
    let key;
    if (fileList != null && fileList.length > 0) {
      key = fileList[0].hash_key;
    } else {
      key = Math.random().toString(36).substring(2, 15);
    }

    const encryptKey = crypto.createHash('sha256').update(key).digest();
    const inputFile = fs.createReadStream(filePath);
    const outputFile = fs.createWriteStream(fileEncryptPath);
    try {
      await inputFile
        .pipe(crypto.createCipheriv(cryptoConst.algorithm, encryptKey, cryptoConst.iv))
        .pipe(outputFile);
    } catch (e) {
      console.log(e);
    }
    return key;
  },

  async saveToDB(file) {
    try {
      if (FileStore.findFileByNameAndEmail(file.name, file.owner).length > 0) {
        FileStore.updateFile(file);
      } else {
        FileStore.addFile(file);
      }
    } catch (e) {
      console.log(e);
    }
  },
};
